from middleware import *

def init_DB(app):
    app.add_url_rule('/api/initdb', 'initDb', init_db, methods=['GET'])

def fill_DB(app):
    app.add_url_rule('/api/filldb', 'fillDb', fill_db, methods=['GET'])

def init_routes_api(app):
    app.add_url_rule('/api/utilisateur', 'api_get_utilisateurs', api_get_utilisateurs, methods=['GET'])
    app.add_url_rule('/api/utilisateur/<string:id>', 'api_get_utilisateur_with_id', api_get_utilisateur_with_id, methods=['GET'])
    app.add_url_rule('/api/utilisateur', 'api_add_utilisateur', api_add_utilisateur, methods=['POST'])
    app.add_url_rule('/api/utilisateur/delete/<string:id>', 'api_delete_utilisateur_with_id', api_delete_utilisateur_with_id, methods=['DELETE'])
    app.add_url_rule('/api/client', 'api_get_clients', api_get_clients, methods=['GET'])
    app.add_url_rule('/api/client/delete/<string:id>', 'api_delete_client_with_id', api_delete_client_with_id, methods=['DELETE'])
    app.add_url_rule('/api/sejour', 'api_get_sejours', api_get_sejours, methods=['GET'])
    app.add_url_rule('/api/sejour/delete/<string:id>', 'api_delete_sejour_with_id', api_delete_sejour_with_id, methods=['DELETE'])
    app.add_url_rule('/api/commande', 'api_get_commandes', api_get_commandes, methods=['GET'])
    app.add_url_rule('/api/commande/client/<string:id>', 'api_get_commandes_by_client', api_get_commandes_by_client, methods=['GET'])

def init_error_handler(app):
    app.errorhandler(404)(error_404)
    app.errorhandler(500)(error_500)
    app.add_url_rule('/crash', 'crash', crash, methods=['GET'])