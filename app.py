from flask import Flask, jsonify, abort, make_response, render_template
from routes import *

app = Flask(__name__)

init_routes_api(app)
init_DB(app)
fill_DB(app)
# init_error_handler(app)

if __name__ == "__main__":
    app.secret_key = 'lealefeuvre'
    app.run(debug=True)
