from sqlalchemy import Column, Integer, String, Date, ForeignKey, Enum
from sqlalchemy.orm import relationship
from models.Model import Model
from models.Categorie import Categorie

class Chambre(Model):
    __tablename__ = 'chambres'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    nom = Column(String(200), nullable=False)
    categorie = Column(Enum(Categorie), nullable=False)

    sejours = relationship("Sejour")
    hotel = Column(Integer, ForeignKey('hotels.id'))

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'categorie': self.categorie.value,
            'sejours': [sjr.serialize() for sjr in self.sejours],
            'hotel': self.hotel
        }