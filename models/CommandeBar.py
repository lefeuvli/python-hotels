from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import relationship
from models.Model import Model

class CommandeBar(Model):
    __tablename__ = 'commandeBars'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    date = Column(Date, nullable=False)
    produits = Column(String(200), nullable=False)

    client = Column(Integer, ForeignKey('clients.id'))
    hotel = Column(Integer, ForeignKey('hotels.id'))

    def serialize(self):
        return {
            'id': self.id,
            'date': self.date,
            'produits': self.produits,
            'client': self.client,
            'hotel': self.hotel
        }