from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import relationship
from models.Model import Model

class Client(Model):
    __tablename__ = 'clients'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    nom = Column(String(200), nullable=False)
    prenom = Column(String(200), nullable=False)
    telephone = Column(String(200), nullable=False)
    carte_bancaire = Column(String(200), nullable=False)
    adresse = Column(String(200), nullable=False)

    sejours =  relationship("Sejour")
    commandesBar = relationship("CommandeBar")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'prenom':self.prenom,
            'telephone': self.telephone,
            'carte_bancaire': self.carte_bancaire,
            'adresse': self.adresse,
            'sejours': [sjr.serialize() for sjr in self.sejours],
            'commandesBar': [cmd.serialize() for cmd in self.commandesBar]
        }