from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.orm import relationship
from models.Model import Model

class Hotel(Model):
    __tablename__ = 'hotels'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    nom = Column(String(200), nullable=False)

    chambres = relationship("Chambre")
    utilisateurs = relationship("Utilisateur")
    commandesBar = relationship("CommandeBar")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'chambres': [chb.serialize() for chb in self.chambres],
            'utilisateurs': [usr.serialize() for usr in self.utilisateurs],
            'commandesBar': [cmd.serialize() for cmd in self.commandesBar]
        }