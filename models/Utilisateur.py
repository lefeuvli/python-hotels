from sqlalchemy import Column, Integer, String, Date, ForeignKey, Boolean, Enum
from sqlalchemy.orm import relationship
from models.Model import Model
from models.Service import Service

class Utilisateur(Model):
    __tablename__ = 'utilisateurs'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    prenom = Column(String(200), nullable=False)
    nom = Column(String(200), nullable=False)
    login = Column(String(200), nullable=False)
    password = Column(String(200), nullable=False)
    telephone = Column(String(20), nullable=False)
    adresse = Column(String(200), nullable=False)
    estActif = Column(Boolean, nullable=False)
    service = Column(Enum(Service), nullable=False)

    hotel = Column(Integer, ForeignKey('hotels.id'))
    sejours = relationship("Sejour")

    def serialize(self):
        return {
            'id':self.id,
            'nom':self.nom,
            'prenom':self.prenom,
            'login':self.login,
            'password':self.password,
            'telephone':self.telephone,
            'adresse':self.adresse,
            'estActif':self.estActif,
            'service': self.service.value,
            'hotel': self.hotel,
            'sejours': [sjr.serialize() for sjr in self.sejours]
        }