from enum import Enum

class Categorie(Enum):
    SIMPLE = 0
    CONFORT = 20
    MOYEN = 40
    JUNIOR = 50
    SENIOR = 60
