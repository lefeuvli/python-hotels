from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import relationship
from models.Model import Model

class Sejour(Model):
    __tablename__ = 'sejours'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    date_arrivee = Column(Date, nullable=False)
    date_depart = Column(Date, nullable=False)

    client = Column(Integer, ForeignKey('clients.id'))
    chambre = Column(Integer, ForeignKey('chambres.id'))
    utilisateur = Column(Integer, ForeignKey('utilisateurs.id'))

    def serialize(self):
        return {
            'id':self.id,
            'date_arrivee':self.date_arrivee,
            'date_depart':self.date_depart,
            'client':self.client,
            'chambre':self.chambre,
            'utilisateur':self.utilisateur
        }