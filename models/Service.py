from enum import Enum

class Service(Enum):
    DIR = "Direction"
    REC = "Réception"
    BAR = "Bar"
    CMP = "Comptabilité"
