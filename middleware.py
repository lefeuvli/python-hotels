from flask import Flask, jsonify, abort, make_response, render_template, flash, request
from provider_service import *

db_engine = 'sqlite:////Users/lea/Documents/database.db'
DATA_PROVIDER = DataProviderService(db_engine)

def index():
    return render_template('about.html')

def init_db():
    DATA_PROVIDER.init_database()
    return jsonify({"response": "init OK"})

def fill_db():
    DATA_PROVIDER.fill_database()
    return jsonify({"response": "fill OK"})

def api_get_utilisateurs():
    utilisateurs = DATA_PROVIDER.get_utilisateurs(serialize=True)
    if utilisateurs:
        return jsonify(utilisateurs)
    else:
        abort(404)

def api_get_utilisateur_with_id(id):
    utilisateur = DATA_PROVIDER.get_utilisateurs(id, serialize=True)
    if utilisateur:
        return jsonify(utilisateur)
    else:
        abort(404)

def api_add_utilisateur():
    utilisateur = DATA_PROVIDER.add_utilisateur(serialize=True)
    if utilisateur:
        return jsonify(utilisateur)
    else:
        abort(404)

def api_delete_utilisateur_with_id(id):
    result = DATA_PROVIDER.delete_utilisateur(id)
    if result:
        return make_response("Suppression OK")
    else:
        abort(404)

def api_delete_client_with_id(id):
    result = DATA_PROVIDER.delete_client(id)
    if result:
        return make_response("Suppression OK")
    else:
        abort(404)

def api_delete_sejour_with_id(id):
    result = DATA_PROVIDER.delete_sejour(id)
    if result:
        return make_response("Suppression OK")
    else:
        abort(404)

def api_get_clients():
    clients = DATA_PROVIDER.get_clients(serialize=True)
    if clients:
        return jsonify(clients)
    else:
        abort(404)
    
def api_get_sejours():
    sejours = DATA_PROVIDER.get_sejours(serialize=True)
    if sejours:
        return jsonify(sejours)
    else:
        abort(404)

def api_get_commandes():
    commandes = DATA_PROVIDER.get_commandes(serialize=True)
    if commandes:
        return jsonify(commandes)
    else:
        abort(404)

def api_get_commandes_by_client(id):
    commandes = DATA_PROVIDER.get_commandes_of_client(id=id, serialize=True)
    if commandes:
        return jsonify(commandes)
    else:
        abort(404)

def crash():
    abort(500)

# ERROR
def error_404(error):
    flash("Sorry, error 404", "error")
    return render_template('404.html')

def error_500(error):
    flash("Sorry, error 500", "error")
    return render_template('500.html')