from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import datetime

from models.Utilisateur import Utilisateur
from models.Chambre import Chambre
from models.Client import Client
from models.CommandeBar import CommandeBar
from models.Hotel import Hotel
from models.Sejour import Sejour
from models.Service import Service
from models.Categorie import Categorie
from models.initDB import init_database

class DataProviderService:
    def __init__(self, engine):
        """
        :param engine: The engine route and login details
        :return: a new instance of DAL class
        :type engine: string
        """
        if not engine:
            raise ValueError('The values specified in engine parameter has to be supported by SQLAlchemy')
        self.engine = engine
        db_engine = create_engine(engine)
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    def init_database(self):
        """
        Initializes the database tables and relationships
        :return: None
        """
        init_database(self.engine)

    def add_utilisateur(self, prenom, nom, login, password, telephone, adresse, service, hotel, serialize=False):
        new_user = Utilisateur(prenom = prenom,
                                nom = nom,
                                login = login,
                                password = password,
                                telephone = telephone,
                                adresse = adresse,
                                service = service,
                                hotel = hotel)

        self.session.add(new_user)
        self.session.commit()
 
        return new_user.serialize()

    def get_utilisateurs(self, id=None, serialize=False):
        all_utiisateurs = []

        if id is None:
            all_utiisateurs = self.session.query(Utilisateur).order_by(Utilisateur.nom).all()
        else:
            all_utiisateurs = self.session.query(Utilisateur).filter(Utilisateur.id == id).all()

        if serialize:
            return [user.serialize() for user in all_utiisateurs]
        else:
            return all_utiisateurs

    def get_clients(self, id=None, serialize=False):
        all_clients = []

        if id is None:
            all_clients = self.session.query(Client).order_by(Client.nom).all()
        else:
            all_clients = self.session.query(Client).filter(Client.id == id).all()

        if serialize:
            return [cli.serialize() for cli in all_clients]
        else:
            return all_clients

    def get_sejours(self, id=None, serialize=False):
        all_sejours = []

        if id is None:
            all_sejours = self.session.query(Sejour).all()
        else:
            all_sejours = self.session.query(Sejour).filter(Sejour.id == id).all()

        if serialize:
            return [sjr.serialize() for sjr in all_sejours]
        else:
            return all_sejours
    
    def get_commandes(self, id=None, serialize=False):
        all_commandes = []

        if id is None:
            all_commandes = self.session.query(CommandeBar).all()
        else:
            all_commandes = self.session.query(CommandeBar).filter(CommandeBar.id == id).all()

        if serialize:
            return [cmd.serialize() for cmd in all_commandes]
        else:
            return all_commandes

    def get_commandes_of_client(self, id, serialize=False):
        all_commandes = []
        
        all_commandes = self.session.query(CommandeBar).filter(CommandeBar.client == id).all()

        if serialize:
            return [cmd.serialize() for cmd in all_commandes]
        else:
            return all_commandes

    def delete_utilisateur(self, id):
        if id:
            items_deleted = self.session.query(Utilisateur).filter(Utilisateur.id == id).delete()
            return items_deleted > 0
        return False

    def delete_client(self, id):
        if id:
            items_deleted = self.session.query(Client).filter(Client.id == id).delete()
            return items_deleted > 0
        return False
    
    def delete_sejour(self, id):
        if id:
            items_deleted = self.session.query(Sejour).filter(Sejour.id == id).delete()
            return items_deleted > 0
        return False

    def fill_database(self):
        #
        # Hotel
        #
        
        hotel = Hotel(nom="Hotel1")

        self.session.add(hotel)
        self.session.commit()

        #
        # Utilisateur
        #
        
        user1 = Utilisateur(prenom = "Prenom1",
                            nom = "Nom1",
                            login = "login1",
                            password = "password1",
                            telephone = "0102030405",
                            adresse = "adresse 1",
                            estActif = True,
                            service = Service.DIR,
                            hotel = hotel.id)

        user2 = Utilisateur(prenom = "Prenom2",
                            nom = "Nom2",
                            login = "login2",
                            password = "password2",
                            telephone = "0102030405",
                            adresse = "adresse 2",
                            estActif = True,
                            service = Service.BAR,
                            hotel = hotel.id)

        self.session.add(user1)
        self.session.add(user2)
        self.session.commit()

        #
        # Client
        #
        
        client1 = Client(nom="NomClient1", prenom= "PrenomClient1", telephone="0102030405", carte_bancaire="3234DF3223", adresse="Adresse1")
        client2 = Client(nom="NomClient2", prenom= "PrenomClient2", telephone="0102030405", carte_bancaire="3234DF3223", adresse="Adresse2")
        
        self.session.add(client1)
        self.session.add(client2)
        self.session.commit()

        #
        # Chambre
        #
        
        chambre1 = Chambre(nom="Chambre1", categorie=Categorie.CONFORT, hotel=hotel.id)
        chambre2 = Chambre(nom="Chambre2", categorie=Categorie.SIMPLE, hotel=hotel.id)
        chambre3 = Chambre(nom="Chambre3", categorie=Categorie.MOYEN, hotel=hotel.id)
        
        self.session.add(chambre1)
        self.session.add(chambre2)
        self.session.add(chambre3)
        self.session.commit()

        #
        # Sejour
        #
        
        sejour1 = Sejour(date_arrivee=datetime.date(2019, 4, 22), 
                        date_depart=datetime.date(2019, 4, 28),
                        client=client1.id,
                        chambre=chambre1.id,
                        utilisateur= user1.id)

        sejour2 = Sejour(date_arrivee=datetime.date(2019, 3, 22), 
                        date_depart=datetime.date(2019, 3, 28),
                        client=client1.id,
                        chambre=chambre2.id,
                        utilisateur= user2.id)
        
        sejour3 = Sejour(date_arrivee=datetime.date(2019, 3, 22), 
                        date_depart=datetime.date(2019, 3, 28),
                        client=client2.id,
                        chambre=chambre3.id,
                        utilisateur= user2.id)
        
        self.session.add(sejour1)
        self.session.add(sejour2)
        self.session.add(sejour3)
        self.session.commit()

        #
        # CommandesBar
        #
        
        commandeBar1 = CommandeBar(date = datetime.date(2019, 5, 10), produits="Produit1;Produit2;Produit3", client=client1.id, hotel= hotel.id)
        commandeBar2 = CommandeBar(date = datetime.date(2019, 5, 14), produits="Produit1;Produit2;Produit3", client=client2.id, hotel= hotel.id)
        
        self.session.add(commandeBar1)
        self.session.add(commandeBar2)
        self.session.commit()